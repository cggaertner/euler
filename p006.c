#include "euler.h"

static euler_uint p006(const euler_uint N)
{
	euler_uint n = N * (N + 1) / 2;
	n *= n;

	for(euler_uint i = 1; i <= N; ++i)
		n -= i * i;

	return n;
}

int main(void)
{
	assert(p006(10) == 2640);
	euler_print_uint(p006(100));
	return 0;
}
