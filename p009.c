#include "euler.h"

static euler_uint p009(const euler_uint N)
{
	euler_uint a, b, c;
	for(a = 1; 3 * a < N; ++a)
	{
		for(b = a + 1; a + 2 * b < N; ++b)
		{
			c = N - a - b;
			if(a * a + b * b == c * c)
				return a * b * c;
		}
	}

	return 0;
}

int main(void)
{
	assert(p009(3 + 4 + 5) == 3 * 4 * 5);
	euler_print_uint(p009(1000));
	return 0;
}
