#include "euler.h"

static euler_uint p003(const euler_uint N)
{
	euler_uint n = N;
	for(euler_uint p = 2;; ++p)
	{
		while(n % p == 0)
			n /= p;

		if(p > n)
			return p;
	}
}

int main(void)
{
	assert(p003(13195) == 29);
	euler_print_uint(p003(600851475143));
	return 0;
}
