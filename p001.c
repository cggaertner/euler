#include "euler.h"

static euler_uint p001(const euler_uint N)
{
	euler_uint sum = 0;

	for(euler_uint n = 1; n < N; ++n)
	{
		if(n % 3 == 0 || n % 5 == 0)
			sum += n;
	}

	return sum;
}

int main(void)
{
	assert(p001(10) == 23);
	euler_print_uint(p001(1000));
	return 0;
}
