#ifndef EULER_H_
#define EULER_H_

#include <assert.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdio.h>

typedef uintmax_t euler_uint;

extern const euler_uint *euler_primes(euler_uint *primes, size_t count);
extern _Bool euler_is_prime(euler_uint value);

static inline void euler_print_uint(euler_uint value)
{
	printf("%" PRIuMAX "\n", value);
}

#endif
