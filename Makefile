.PHONY : build clean run

CLANG := clang -std=c99 -Werror -Weverything
GCC := gcc -std=c99 -Werror -Wall -Wextra
CFLAGS := -O3
NOWARN := format-invalid-specifier

CHECK_SYNTAX = $(CLANG) -fsyntax-only $(NOWARN:%=-Wno-%) $<
COMPILE = $(GCC) -c $(CFLAGS) -o $@ $<
BUILD = $(GCC) $(CFLAGS) -o $@ $^
CLEAN = rm -f $(GARBAGE)
RUN = @set -e; for BIN in $^; do echo ./$$BIN; ./$$BIN; done

PROBLEMS := $(sort $(wildcard p*.c))
BINARIES := $(PROBLEMS:%.c=%)
OBJECTS := euler.o
GARBAGE := $(OBJECTS) $(BINARIES)

build : $(BINARIES)

clean :
	$(CLEAN)

run : $(BINARIES)
	$(RUN)

$(BINARIES) : % : %.c $(OBJECTS)
	$(CHECK_SYNTAX)
	$(BUILD)

p021 : NOWARN += float-equal
$(OBJECTS) : %.o : %.c euler.h
	$(CHECK_SYNTAX)
	$(COMPILE)
