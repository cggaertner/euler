#include "euler.h"

static euler_uint p007(const size_t N)
{
	euler_uint primes[N];
	return euler_primes(primes, N)[N - 1];
}

int main(void)
{
	assert(p007(6) == 13);
	euler_print_uint(p007(10001));
	return 0;
}
