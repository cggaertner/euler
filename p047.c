#include "euler.h"

#define CACHE_SIZE (1 << 18)

static euler_uint factors(euler_uint n)
{
	static euler_uint cache[CACHE_SIZE];

	if(n < 2)
		return 0;

	assert(n < CACHE_SIZE);

	if(cache[n])
		return cache[n];

	for(euler_uint p = 2; p * p <= n; ++p)
	{
		if(n % p == 0)
		{

			euler_uint r = n / p;
			while(r % p == 0)
				r /= p;

			cache[n] = 1 + factors(r);
			return cache[n];
		}
	}

	cache[n] = 1;
	return cache[n];
}

static euler_uint p047(const euler_uint N)
{
	euler_uint n = 2;

	for(euler_uint count = 0; count != N; ++n)
	{
		if(factors(n) == N)
			++count;
		else count = 0;
	}

	return n - N;
}

int main(void)
{
	assert(p047(2) == 14);
	assert(p047(3) == 644);
	euler_print_uint(p047(4));
	return 0;
}
