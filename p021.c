#include "euler.h"
#include <math.h>

static euler_uint d(euler_uint n)
{
	euler_uint sum = 1;
	double sqrt_n = sqrt(n);
	euler_uint A = (euler_uint)sqrt_n;

	for(euler_uint a = 2; a <= A; ++a)
	{
		if(n % a == 0)
			sum += a + n / a;
	}

	return (double)A == sqrt_n ? sum - A : sum;
}

static euler_uint p021(const euler_uint N)
{
	euler_uint sum = 0;

	for(euler_uint a = 1; a <= N; ++a)
	{
		euler_uint b = d(a);
		if(a < b && d(b) == a)
			sum += a + b;
	}

	return sum;
}

int main(void)
{
	assert(p021(500) == 220 + 284);
	euler_print_uint(p021(10000));
	return 0;
}
