#include "euler.h"
#include <string.h>

static const euler_uint KNOWN_PRIMES[] = {
	2,
	3,
	5,
	7
};

static const size_t KNOWN_PRIMES_COUNT =
	sizeof KNOWN_PRIMES / sizeof *KNOWN_PRIMES;

const euler_uint *euler_primes(euler_uint *primes, size_t count)
{
	if(count <= KNOWN_PRIMES_COUNT)
		return KNOWN_PRIMES;

	memcpy(primes, KNOWN_PRIMES, sizeof KNOWN_PRIMES);

	euler_uint *prime = primes + KNOWN_PRIMES_COUNT;
	euler_uint *const END = primes + count;

	euler_uint candidate = KNOWN_PRIMES[KNOWN_PRIMES_COUNT - 1];
	while(prime < END)
	{
	NEXT_CANDIDATE:
		candidate += 2;

		for(euler_uint *cp = primes + 1; cp < prime; ++cp)
		{
			if(candidate % *cp == 0)
				goto NEXT_CANDIDATE;

			if(*cp * *cp > candidate)
				break;
		}

		*prime++ = candidate;
	}

	return primes;
}

_Bool euler_is_prime(euler_uint value)
{
	if(value % 2 == 0)
		return 0;

	for(euler_uint n = 3; n * n <= value; n += 2)
	{
		if(value % n == 0)
			return 0;
	}

	return 1;
}
